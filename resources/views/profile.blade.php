@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-success">
                    <div class="card-body px-5">
                        <h1 class="card-title text-center mb-5">{{ __('auth.profile') }}</h1>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">ID #</th>
                                <th scope="col">Email</th>
                                <th scope="col">Created At</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">{!! session('user.id') !!}</th>
                                <td>{!! session('user.email') !!}</td>
                                <td>{!! session('user.created_at') !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
