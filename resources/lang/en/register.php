<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Register',
    'sub-title' => 'and create your account',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'code' => 'Code',
    'email' => 'Email address',
    'password' => 'Password',
    'confirm' => 'Confirm Password',
    'btn' => 'Register',
    'agreement' => 'I agree with <a href="#">Terms & Conditions</a> and <a href="#">Privacy policy</a>',
    'login' => 'Log in',


];
