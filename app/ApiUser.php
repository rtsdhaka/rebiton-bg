<?php

namespace App;

use Illuminate\Support\Collection;
use Illuminate\Support\Fluent;
use Illuminate\Contracts\Auth\Authenticatable;

class ApiUser extends Fluent implements Authenticatable
{
    protected $attributes;

    /**
     * Get the attributes from the fluent instance.
     *
     * @return array
     */
    public function getAttributes()
    {
        $this->attributes = Fluent::getAttributes();
        return $this->attributes;
    }


    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        /*if(session()->has('user.id')) {
            $this->id = session('user.id');
        }
        return $this->id;*/
        return $this->email;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {

    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {

    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {

    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        // TODO: Implement retrieveById() method.


        $qry = UserPoa::where('admin_id','=',$identifier);

        if($qry->count() >0)
        {
            $user = $qry->select('admin_id', 'username', 'first_name', 'last_name', 'email', 'password')->first();

            $attributes = array(
                'id' => $user->admin_id,
                'username' => $user->username,
                'password' => $user->password,
                'name' => $user->first_name . ' ' . $user->last_name,
            );

            return $user;
        }
        return null;
    }

    /**
     * Retrieve a user by by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        // TODO: Implement retrieveByToken() method.
        $qry = UserPoa::where('admin_id','=',$identifier)->where('remember_token','=',$token);

        if($qry->count() >0)
        {
            $user = $qry->select('admin_id', 'username', 'first_name', 'last_name', 'email', 'password')->first();

            $attributes = array(
                'id' => $user->admin_id,
                'username' => $user->username,
                'password' => $user->password,
                'name' => $user->first_name . ' ' . $user->last_name,
            );

            return $user;
        }
        return null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        // TODO: Implement updateRememberToken() method.
        $user->setRememberToken($token);

        $user->save();

    }

}
