<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\HttpHelper;
use App\ApiUser as User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request)
    {
        $request->validate( [
            'code' => ['required', 'string', 'min:15', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'agreement' => ['boolean'],
        ]);
        // Send api request to authenticate
        $http = new HttpHelper();
        $params = [
            'code' => $request->code,
            'email' => $request->email,
            'password' => $request->password,
            'agreement' => $request->agreement,
        ];
        $body = $http->register($params);

        if(array_key_exists('data', $body)) {
            // to get token, try login
            $credentials = [
                'email' => $request->email,
                'password' => $request->password,
            ];
            $loginresponse = $http->login($credentials);
            if(array_key_exists('data', $loginresponse)) {
                $userData = $http->getUser($loginresponse['data']['token']);
                if(array_key_exists('data', $userData)) {
                    $data = [
                        'id' => $userData['data']['id'],
                        'name' => $userData['data']['email'],
                        'email' => $userData['data']['email'],
                        'email_verified_at' => $userData['data']['email_verified_at'],
                        'phone' => $userData['data']['phone'],
                        'phone_verified_at' => $userData['data']['phone_verified_at'],
                        'created_at' => $userData['data']['created_at'],
                        'country' => $userData['data']['country'],
                        'locale'  => $userData['data']['locale'],
                        'token' => $loginresponse['data']['token']
                    ];
                    $user = factory(\App\User::class)->make($data);

                    auth()->login($user);
                    session()->put(['user' => auth()->user()]);
                }
            }
        } else {
            return response()->json($body, 422);
        }

        return response()->json(['redirect' => session()->pull('url.intended', $this->redirectTo)]);
    }
}
