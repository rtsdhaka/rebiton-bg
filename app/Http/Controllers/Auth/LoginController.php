<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\HttpHelper;
use App\Http\Controllers\Controller;
use App\ApiUser as User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login user
     *
     * @param  [string] email
     * @param  [string] password
     * @return [json] redirect
     */

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6'
        ]);
        $params = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        // Send api request to authenticate
        $http = new HttpHelper();
        $body = $http->login($params);
        if(array_key_exists('data', $body)) {
            $userData = $http->getUser($body['data']['token']);
            if(array_key_exists('data', $userData)) {
                $data = [
                    'id' => $userData['data']['id'],
                    'name' => $userData['data']['email'],
                    'email' => $userData['data']['email'],
                    'email_verified_at' => $userData['data']['email_verified_at'],
                    'phone' => $userData['data']['phone'],
                    'phone_verified_at' => $userData['data']['phone_verified_at'],
                    'created_at' => $userData['data']['created_at'],
                    'country' => $userData['data']['country'],
                    'locale'  => $userData['data']['locale'],
                    'token' => $body['data']['token']
                ];
                $user = factory(\App\User::class)->make($data);

                auth()->login($user);
                session()->put(['user' => auth()->user()]);
            }
        }

        return response()->json(['redirect' => session()->pull('url.intended', $this->redirectTo)]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        //$request->user()->token()->revoke();
        session()->flush();
        return redirect()->intended( $this->redirectTo);
    }
}
