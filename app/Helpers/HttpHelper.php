<?php


namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;


class HttpHelper
{
    protected $request;
    protected $client;

    /**
     * Constructor
     */
    public function __construct(Request $request = null)
    {
        $this->request  =   $request;
        $this->client   =   new Client(['http_errors'=>false, 'verify' => false ]);

    }

    /**
     * Login Method
     * @Parameters
     * 1	email *	string (max 255)
     * 2	password *	string (min 6)
     * @return array
     */
    public function login($params){
        $response = $this->client->post(config('settings.api.url').'/auth/login', [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'json' => $params,
        ]);

        $body = json_decode($response->getBody(true), true);

        return $body;
    }

    /**
     * Registration Method
     * @Parameters
     * 1	code *	string (min: 15 max: 50)
     * 2	email *	string (max 255)
     * 3	password *	string (min 6)
     * 4    agreement * boolean
     * @return
     */
    public function register($params){

        try
        {
            $response = $this->client->post(config('settings.api.url').'/auth/register', [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'json' => $params,
            ]);
            $body = json_decode($response->getBody(true), true);
            return $body;
        } catch (ClientException $e) {
            //$response = new \stdClass();
            //$response->error = $e->getMessage();
            return $e;
        }

    }

    /*
     * Get authenticated user's info
     * 1	token *	string
     * @return User data
     */

    public function getUser($token) {

        $response = $this->client->get(config('settings.api.url').'/user', [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
                'Accept' => 'application/json',
            ],
        ]);

        if($response->getStatusCode() == 200) {
            $body = json_decode($response->getBody(true), true);
            return $body;
        }

        return [];
    }

    /*
     * Get authenticated user's info
     * 1	token *	string
     * @return User data
     */

    public function getLocal() {
        $data = [ 'language' => 'en' ];
        $response = $this->client->get(config('settings.api.url').'/language', [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        if($response->getStatusCode() == 200) {
            $body = json_decode($response->getBody(true), true);
            if(array_key_exists('data', $body)) {
                $data = $body['data'];
            }
        }

        return $data;
    }
}
